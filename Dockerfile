# Set up a bootstrap image with the basic runtime requirements
FROM ubuntu:19.04 AS bootstrap

ENV \
  DEBIAN_FRONTEND=noninteractive \
  HOME=/home/gdle \
  USER=gdle \
  UID=1000

RUN apt update \
  && apt install -y \
    libmysqlclient20 \
    mariadb-client-core-10.3 \
    mariadb-client-10.3

RUN useradd --no-create-home --home-dir "${HOME}" --uid "${UID}" "${USER}"

# Set up a builder container that will actually compile GDLEnhanced
FROM ubuntu:19.04 AS builder

ENV \
  DEBIAN_FRONTEND=noninteractive \
  HOME=/home/gdle \
  USER=gdle \
  UID=1000

RUN apt update \
  && apt install -y \
    build-essential \
    cmake \
    cmake-data \
    coreutils \
    libmysqlclient-dev

RUN useradd --no-create-home --home-dir "${HOME}" --uid "${UID}" "${USER}"

ADD gdlenhanced "${HOME}"

RUN cd "${HOME}/Bin" \
  && rm -rf CMake* cmake* \
  && cmake .. \
  && make clean \
  && make --jobs "$(nproc --all)"

# Load the bootstrapped runtime container and finalize it
FROM bootstrap

COPY --from=builder "${HOME}/Bin" "${HOME}"

RUN chown -R "${USER}" "${HOME}"

COPY ./entrypoint.sh "${HOME}/entrypoint.sh"

USER "${USER}"

ENTRYPOINT ["/home/gdle/entrypoint.sh"]

CMD ["/home/gdle/GDLEnhanced"]
