#!/usr/bin/env bash

THIS_DIR="$(cd "$(dirname "${BASH_SOURCE[0]}")" && pwd)"

main() {
  . "${THIS_DIR}/.env"

  local DB="${MYSQL_DATABASE-gdle}"
  local PW="${MYSQL_PASSWORD-123}"
  local U="${MYSQL_USER-gdle}"

  set -x

  cat "${DB}.sql.gz" | gzip -d | docker-compose exec -T mariadb mysql -u "${U}" -h 127.0.0.1 -p${PW} "${DB}"
}

main "$@"
