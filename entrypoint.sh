#!/usr/bin/env bash

THIS_DIR="$(cd "$(dirname "${BASH_SOURCE[0]}")" && pwd)"

main() {
  local CFG_EX="config/example.cfg"
  local CFG="config/${CONFIG_FILE-server.cfg}"
  local i=0

  set -x

  for i in {0..60..2}; do
    mysql \
      -u ${MYSQL_USER} \
      -p${MYSQL_PASSWORD} \
      -h mariadb \
      ${MYSQL_DATABASE} \
      -e 'SELECT 1 FROM accounts' \
      && break

    sleep 2
  done

  if [ ! -f "${CFG}" ]; then
    cp "${CFG_EX}" "${CFG}"
  fi

  exec "$@"
}

main "$@"
