#!/usr/bin/env bash

THIS_DIR="$(cd "$(dirname "${BASH_SOURCE[0]}")" && pwd)"

main() {
  . "${THIS_DIR}/.env"

  local DB="${MYSQL_DATABASE-gdle}"
  local PW="${MYSQL_PASSWORD-123}"
  local U="${MYSQL_USER-gdle}"

  docker-compose exec -T mariadb mysqldump -u "${U}" -h 127.0.0.1 -p${PW} --databases "${DB}" | gzip -9 > "${DB}.sql.gz"
}

main "$@"
